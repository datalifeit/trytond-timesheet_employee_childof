# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['TeamEmployee']


class TeamEmployee(metaclass=PoolMeta):
    __name__ = 'company.employee.team-company.employee'

    @classmethod
    def __setup__(cls):
        super(TeamEmployee, cls).__setup__()
        cls.employee.domain = [
               ('company', 'child_of', Eval('_parent_team', {}).get(
                'company', None), 'parent')] + cls.employee.domain[1:]

